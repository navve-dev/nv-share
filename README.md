# NvShare

Small script to allow user to share an url. It uses the browser's native share behavior if available. Otherwise, it adds a panel in page with share links to Facebook, Twitter, Whatsapp and email.

## How to use

Just add the script to your project. It will enable the share functionality in any element with `share-trigger` class.

### Data attributes

The element can have the following attributes:

* **data-url** (required): Url to share.
* **data-text** (optional): Text to send along (only for Twitter and e-mail).
* **data-title** (optional): Title of page to share (only for e-mail).

### CSS

The site must have css code to show the share panel properly.

## Author

Coded by [Lanza](mailto:fabio.lanzarin@portotech.org).