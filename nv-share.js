(function (root, doc) {

    const stopPropagation = function (evt) {
        evt.stopPropagation();
    };

    const hideSharePanel = function (evt) {
        const panel = doc.querySelector('.nv-share-panel');
        if(panel) {
            panel.classList.remove('active');
        }
        evt.currentTarget.removeEventListener('click', hideSharePanel);
    };

    const showSharePanel = function (data) {
        const url = encodeURIComponent(data.url);
        const title = encodeURIComponent(data.title);
        const text = encodeURIComponent(data.text);
        const body = doc.querySelector('body');
        let panel = body.querySelector('.nv-share-panel');
        if(!panel) {
            panel = doc.createElement('div');
            panel.className = 'nv-share-panel';
            panel.innerHTML = [
                '<div class="nv-share-panel-content"><h1>Escolha como quer compartilhar</h1><ul>',
                '<li class="nv-share-whatsapp"><a href="#" target="_blank"><i class="ic-whatsapp"></i> <span>Whatsapp</span></a>',
                '<li class="nv-share-facebook"><a href="#" target="_blank"><i class="ic-facebook"></i> <span>Facebook</span></a>',
                '<li class="nv-share-twitter"><a href="#" target="_blank"><i class="ic-twitter"></i> <span>Twitter</span></a>',
                '<li class="nv-share-email"><a href="#" target="_blank"><i class="ic-mail"></i> <span>E-mail</span></a>',
                '</ul></div>'
            ].join('');
            body.appendChild(panel);
            panel.querySelector('.nv-share-panel-content').addEventListener('click', stopPropagation);
        }
        panel.querySelector('.nv-share-whatsapp a').setAttribute('href', 'whatsapp://send?text=' + url);
        panel.querySelector('.nv-share-facebook a').setAttribute('href', 'http://www.facebook.com/sharer.php?u=' + url);
        panel.querySelector('.nv-share-twitter a').setAttribute('href', 'https://twitter.com/intent/tweet?original_referer=' + url + '&text=' + text + '&tw_p=tweetbutton&url=' + url);
        panel.querySelector('.nv-share-email a').setAttribute('href', 'mailto:?subject=' + title + '&body=' + text + '  ' + url);
        body.removeEventListener('click', hideSharePanel);
        body.addEventListener('click', hideSharePanel);
        root.setTimeout(() => {
            panel.classList.add('active');
        }, 100);
    };

    const startShare = function (evt) {
        const element = evt.currentTarget;
        const data = {
            title: element.getAttribute('data-title') || '',
            text: element.getAttribute('data-text') || '',
            url: element.getAttribute('data-url') || root.location.href
        };
        if(root.navigator && root.navigator.share) {
            root.navigator.share(data);
        } else {
            showSharePanel(data);
        }
    };

    const handleDocClick = function (evt) {
      const triggers = doc.querySelectorAll('.share-trigger[data-url]');
      const el = [...triggers].find(trigger => evt.target === trigger || trigger.contains(evt.target));
      if(el) {
          startShare({ currentTarget: el });
      }
    };

    const init = function () {
        doc.addEventListener('click', handleDocClick);
    };

    if(doc.readyState === 'loading') {
        doc.addEventListener('DOMContentLoaded', init);
    } else {
        init();
    }

})(window, document);